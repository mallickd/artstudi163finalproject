class NoisyCircle {
  float resolution = 260;
  float r;

  float x;
  float y;
  float dx;
  float dy;
  
  float t = 0;
  float dt = 0.01;
  float dr = 0.5;

  public NoisyCircle(float xIn, float yIn, float rIn, float dxIn, float dyIn) {
    x = xIn;
    y = yIn;
    r = rIn;
    dx = dxIn;
    dy = dyIn;
  }

  public void draw() {
    float nInt = map(0, 0, width, 0.1, 30); // map mouseX to noise intensity
    float nAmp = map(0, 0, height, 0.0, 1.0); // map mouseY to noise amplitude

    beginShape();
    for (float a = 0; a <= TWO_PI; a += TWO_PI/resolution) {
      float nVal = map(noise(cos(a)*nInt+1, sin(a)*nInt+1, t), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude
      float offsetX = cos(a) * r * nVal;
      float offsetY = sin(a) * r * nVal;
      vertex(x + offsetX, y + offsetY);
    }
    endShape(CLOSE);
    
    t += dt;
    r += dr;
  }

  public void move() {
    x += dx;
    y += dy;

    if (x > width || x < 0) {
      dx *= -1;
    }
    if (y > height || y < 0) {
      dy *= -1;
    }
  }
}