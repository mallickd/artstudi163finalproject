NoisyCircle[] ncs;
int numCircles = 80;
int circleRadius = 75;
int framesSaved = 0;

public void setup() {
  size(600, 600);
  background(28,163,236);
  noFill();
  stroke(240,248,255, 30);
  
  ncs = new NoisyCircle[numCircles];
  for (int i = 0; i < ncs.length; i++) {
    ncs[i] = new NoisyCircle(random(width), random(height), circleRadius, 0, 0);
  }
}

public void draw() {
  background(28,163,236, 5);
  for (int i = 0; i < ncs.length; i++) {
    ncs[i].draw();
  }
}

public void keyPressed() {
  if(key=='s'){
    save("commit" + framesSaved++ + ".tif"); //save to an external file
  }
}