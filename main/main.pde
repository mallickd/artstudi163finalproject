import java.util.List;

class NoisyCircle {
  float resolution = 260;
  float r;

  float x;
  float y;
  float dx = random(rate_floor, rate_ceil);
  float dy = random(rate_floor, rate_ceil);
  int id;
  NoisyCircle[] others;

  public NoisyCircle(float xIn, float yIn, float rIn, int idIn, NoisyCircle[] oIn) {
    x = xIn;
    y = yIn;
    r = rIn;
    id = idIn;
    others = oIn;
  }
  
  public void collide() {
    for (int i = id + 1; i < others.length; i++) {
      float sepX = others[i].x - x;
      float sepY = others[i].y - y;
      float distance = sqrt(sepX*sepX + sepY*sepY);
      float minDist = others[i].r + r;
      if (distance < minDist) {
        float angle = atan2(sepY, sepX);
        float targetX = x + cos(angle) * minDist;
        float targetY = y + sin(angle) * minDist;
        float ax  = (targetX - others[i].x) * spring;
        float ay = (targetY - others[i].y) * spring;
        dx = (dx - ax >= 0)? min(rate_ceil, dx - ax) : max(rate_floor, dx - ax);
        dy = (dy - ay >= 0)? min(rate_ceil, dy - ay) : max(rate_floor, dy - ay);
        others[i].dx = (others[i].dx + ax >= 0)? min(rate_ceil, others[i].dx + ax) : max(rate_floor, others[i].dx + ax);
        others[i].dy = (others[i].dy + ay >= 0)? min(rate_ceil, others[i].dy + ay) : max(rate_floor, others[i].dy + ay);
      }
    }
  }

  public void draw() {
    float nInt = map(0, 0, width, 0.1, 30); // map mouseX to noise intensity
    //float nAmp = map(height, 0, height, 0.0, 1.0); // map mouseY to noise amplitude
    float nAmp = 0.9;

    beginShape();
    for (float a = 0; a <= TWO_PI; a += TWO_PI/resolution) {
      float nVal = map(noise(cos(a)*nInt+1, sin(a)*nInt+1, t), 0.0, 1.0, nAmp, 1.0); // map noise value to match the amplitude
      float offsetX = cos(a) * r * nVal;
      float offsetY = sin(a) * r * nVal;
      vertex(x + offsetX, y + offsetY);
    }
    endShape(CLOSE);
  }

  public void move() {
    x += dx;
    y += dy;

    if (x > width || x < 0) {
      dx *= -1;
    }
    if (y > height || y < 0) {
      dy *= -1;
    }
  }
}


/*
 * Portions of this class influenced by Processing example Bouncy Bubbles
 */
class Circle {
  float x, y, r;
  float dx = random(rate_floor, rate_ceil);
  float dy = random(rate_floor, rate_ceil);
  int id;
  Circle[] others;
  
  float minDistScale = 0.6;

  Circle(float xin, float yin, float rin, int idin, Circle[] oin) {
    x = xin;
    y = yin;
    r = rin;
    id = idin;
    others = oin;
  }
  
  void collide(boolean reboundFromCircle) {
    for (int i = id + 1; i < others.length; i++) {
      /* collision variables init */
      float sepX = others[i].x - x;
      float sepY = others[i].y - y;
      float distance = sqrt(sepX*sepX + sepY*sepY);
      float intersectMinDist = (others[i].r + r);
      float reboundMinDist = minDistScale * (others[i].r + r);
      
      /* intersection */
      if (distance < intersectMinDist) {
        /* handle intersection */
        float[] intersection = circleCircleIntersects(x, y, others[i].x, others[i].y, r, others[i].r);
        if (intersection != null) {
          //stroke(160,204,227, 10);
          stroke(255,255,255);
          line(intersection[0], intersection[1], intersection[2], intersection[3]);
          noStroke();
        }
      }
      
      /* rebound */
      if (distance < reboundMinDist) {
        /* handle rebound */
        if (reboundFromCircle) {
          float angle = atan2(sepY, sepX);
          float targetX = x + cos(angle) * reboundMinDist;
          float targetY = y + sin(angle) * reboundMinDist;
          float ax = (targetX - others[i].x) * spring;
          float ay = (targetY - others[i].y) * spring;
          dx = (dx - ax >= 0)? min(rate_ceil, dx - ax) : max(rate_floor, dx - ax);
          dy = (dy - ay >= 0)? min(rate_ceil, dy - ay) : max(rate_floor, dy - ay);
          others[i].dx = (others[i].dx + ax >= 0)? min(rate_ceil, others[i].dx + ax) : max(rate_floor, others[i].dx + ax);
          others[i].dy = (others[i].dy + ay >= 0)? min(rate_ceil, others[i].dy + ay) : max(rate_floor, others[i].dy + ay);
        }
      }
    }
  }

  void move(boolean reboundFromWall) {
    x += dx;
    y += dy;
    if (reboundFromWall) {
      if (x > width || x < 0) dx*=-1;
      if (y > height || y < 0) dy*=-1;
    }
  }
  
  void reverse() {
    dx *= -1;
  }

  void draw() {
    ellipse(x, y, r, r);
  }
}

int NUM_LARGE_CIRCLES = 240;
int NUM_SMALL_CIRCLES = 150;
float LARGE_CIRCLE_RADIUS = 25;
float SMALL_CIRCLE_RADIUS = 8;
float rate_ceil = .5;
float rate_floor = -.5;
//UNCOMMENT IF WANT NOISYCIRCLES
//NoisyCircle[] ncs;
Circle[] circles;
float spring = 0.05;
int t = 0;
int framesSaved = 0;
PImage bg;

void setup() {
  //Init
  size(600, 600);
  noiseDetail(8);
  frameRate(40);
  ellipseMode(RADIUS);
  //background(28,163,236);
  bg = loadImage("backgrounds/background1.tif");
  //background(bg);
  
  
  //fill(28,163,236);
  //stroke(28,163,236);
  noStroke();
  noFill();

  //UNCOMMENT IF WANT NOISYCIRCLES
  //ncs = initCircles();
  circles = initCircles();
}

//UNCOMMENT IF WANT NOISYCIRCLES
//NoisyCircle[] initCircles() {
//  NoisyCircle[] ncsIn = new NoisyCircle[NUM_LARGE_CIRCLES + NUM_SMALL_CIRCLES];
//  for (int i = 0; i < NUM_LARGE_CIRCLES + NUM_SMALL_CIRCLES; i++) {
//    //large circles
//    if (i < NUM_LARGE_CIRCLES) {
//      ncsIn[i] = new NoisyCircle(random(width), random(height), LARGE_CIRCLE_RADIUS, i, ncsIn);
//    } else {
//      ncsIn[i] = new NoisyCircle(random(width), random(height), SMALL_CIRCLE_RADIUS, i, ncsIn);
//    }
//  }
  
//  return ncsIn;
//}

Circle[] initCircles() {
  Circle[] cIn = new Circle[NUM_LARGE_CIRCLES + NUM_SMALL_CIRCLES];
  for (int i = 0; i < NUM_LARGE_CIRCLES + NUM_SMALL_CIRCLES; i++) {
    //large circles
    if (i < NUM_LARGE_CIRCLES) {
      cIn[i] = new Circle(random(width), random(height), LARGE_CIRCLE_RADIUS, i, cIn);
    } else {
      cIn[i] = new Circle(random(width), random(height), SMALL_CIRCLE_RADIUS, i, cIn);
    }
  }
  
  return cIn;
}

/**
 * Function adapted from OpenProcessing "Circle Circle Intersection" 
 * Finds the intersections of two circles, if they exist.
 *
 * Given two circle equations:
 *   Cirlce 1: r1^2 = (x - x1)^2 + (y - y1)^2
 *   Circle 2: r2^2 = (x - x2)^2 + (y - y2)^2
 *
 * Returns coordinates (2 sets of 2 doubles) or else returns null if intersections do not exist.
 *
 * Rarely this function may return null if the circles are tangental or very close to tangental.
 */
public double[] circleCircleIntersects(double x1, double y1, double x2, double y2, double r1, double r2)
{
  // Use change of coordinates to get:
  //   Cirlce 1: r1^2 = x^2 + y^2
  //   Circle 2: r2^2 = (x - a)^2 + (y - b)^2
  double a = x2 - x1;
  double b = y2 - y1;
  
  // Find distance between circles.
  double ds = a*a + b*b;
  double d = Math.sqrt( ds );
  
  // Ensure that the combined radii lengths are longer than the distance between the circles,
  // i.e. tha the circles are close enough to intersect.
  if (r1 + r2 <= d)
    return null;
  
  // Ensure that one circle is not inside the other.
  if (d <= Math.abs( r1 - r2 ))
    return null;
  
  // Find the intersections (formula derivations not shown here).
  double t = Math.sqrt( (d + r1 + r2) * (d + r1 - r2) * (d - r1 + r2) * (-d + r1 + r2) );

  double sx1 = 0.5 * (a + (a*(r1*r1 - r2*r2) + b*t)/ds);
  double sx2 = 0.5 * (a + (a*(r1*r1 - r2*r2) - b*t)/ds);
  
  double sy1 = 0.5 * (b + (b*(r1*r1 - r2*r2) - a*t)/ds);
  double sy2 = 0.5 * (b + (b*(r1*r1 - r2*r2) + a*t)/ds);
  
  // Translate to get the intersections in the original reference frame.
  sx1 += x1;
  sy1 += y1;
  
  sx2 += x1;
  sy2 += y1;
  
  double[] r = new double[4];
  r[0] = sx1;
  r[1] = sy1;
  r[2] = sx2;
  r[3] = sy2;
  
  return r;
}

float[] circleCircleIntersects(float x1, float y1, float x2, float y2, float r1, float r2) {
  double[] intersection = circleCircleIntersects((double) x1, (double) y1, (double) x2, (double) y2, (double) r1, (double) r2);
  if (intersection == null) {
    return null;
  } else {
    float[] floatIntersection = new float[intersection.length];
    for (int i = 0; i < intersection.length; i++) {
      floatIntersection[i] = (float) intersection[i];
    }
    return floatIntersection;
  }
}

double timeRemaining = 2.0;
double trigger = 2.0;
int reverseTime = 50;
int timeToSeparate = 500;

void initEbbFlow() {
  for (Circle c : circles) {
    c.dx = 0.5;
    c.dy = 0;
  }
}

void draw() {
  background(bg);
  if (t < timeToSeparate) {
    /* Allow circles to separate */
    drawCircles(true);
  } else if (t == timeToSeparate) {
    /* modify circles fields to begin ebb & flow */
    initEbbFlow();
  } else {
    /* Now begin tide animation */
    drawCircles(false);
  }
  t++;
}

void drawCircles(boolean rebound) {
  for (Circle c : circles) {
    c.collide(rebound);
    c.move(rebound);
    c.draw();
  }
}

//void draw() {
//  background(bg);
  
//  //UNCOMMENT IF WANT NOISYCIRCLES
//  //for (NoisyCircle c1 : ncs) {
//  for (Circle c1 : circles) {
//    c1.collide();
//    c1.move();
//    /* ebb -> flow, flow -> ebb */
//    if ((t % reverseTime) == 0) {
//      c1.reverse();
//    }
    
//    c1.draw();
//  }
//  t++;
//}

void keyPressed(){
  if(key=='s'){
    save("commit" + framesSaved++ + ".tif"); //save to an external file
  }
}